var firebaseConfig = {
    apiKey: "AIzaSyA_OsmWvnB4bRlzqOKzAyiXuR2xq_6rImQ",
    authDomain: "myhostpital.firebaseapp.com",
    databaseURL: "https://myhostpital.firebaseio.com",
    projectId: "myhostpital",
    storageBucket: "myhostpital.appspot.com",
    messagingSenderId: "786485289225",
    appId: "1:786485289225:web:4383a9500013a0202eb3d8",
    measurementId: "G-GZS5K9TB9K"
};
// Initialize Firebase

firebase.initializeApp(firebaseConfig);
var db = firebase.firestore();
db.collection("hospitals")
    .orderBy("bed_number")
    .get()
    .then((querySnapshot) => {
        let content = ``;
        let select = ``;
        querySnapshot.forEach((doc) => {
            let item = doc.data();
            content += `<tr>
                <td><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal3" onclick="renderPatient('${doc.id}')" >${item.name}</button></td>
                <td>${item.address}</td>
                <td><img src="${item.logo}" width="60"></td>
                <td>${item.bed_number}</td>
                <td><button class="btn btn-primary" onclick="deleteHospital('${doc.id}')">Xoá</button></td>
                <td><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal2" onclick="updateHospital('${doc.id}')" >Sửa</button></td>
            </tr>`
            select += `<option value="${doc.id}">${item.name}</option>`;

        });
        document.querySelector('tbody[class="hospital"]').innerHTML = content;
        document.querySelector('select[name="hospital_id"]').innerHTML = select;

    });

function saveHospital() {
    if (document.querySelector('input[name="name"]').value == "") {
        $('.errorName').append('<p>Bạn cần nhập tên bệnh viện</p>');
    } else if (document.querySelector('input[name="address"]').value == "") {
        $('.errorAddress').append('<p>Bạn cần nhập địa chỉ</p>')

    } else if (document.querySelector('input[name="bed_number"]').value == "") {
        $('.errorBed_Number').append('<p>Bạn cần nhập số giường bệnh</p>')

    } else if (document.querySelector('input[name="logo"]').value == "") {
        $('.errorlogo').append('<p>Bạn cần nhập logo</p>');
    } else {
        let data = {
            name: document.querySelector('input[name="name"]').value,
            address: document.querySelector('input[name="address"]').value,
            bed_number: document.querySelector('input[name="bed_number"]').value,
            logo: document.querySelector('input[name="logo"]').value,
        };
        var newHospital = db.collection("hospitals").doc();
        newHospital.set(data);
        document.querySelector('input[name="name"]').value = "";
        document.querySelector('input[name="address"]').value = "";
        document.querySelector('input[name="bed_number"]').value = "";
        document.querySelector('input[name="logo"]').value = "";
    }

}




function deleteHospital(key) {
    db.collection("hospitals").doc(key).delete().then(function() {
        db.collection("patients").where("hospital", "==", key).delete().then(function() {
            location.reload();
        })

    })



}

function updateHospital(key) {
    var docRef = db.collection("hospitals").doc(key);

    docRef.get().then(function(doc) {
        content = ``;
        item = doc.data();
        content = `<legend>Tạo mới bệnh viện</legend>
        <div class="form-group">
            <label for="">Tên bệnh viện</label>
            <input type="text" name="editName" class="form-control" value="${item.name}">
        </div>
        <div>
            <label for="">Địa chỉ bệnh viện</label>
            <input type="text" name="editAddress" class="form-control" value="${item.address}">
        </div>
        <div>
            <label for="">logo</label>
            <input type="text" name="editLogo" class="form-control" value="${item.logo}">
        </div>
        <div>
            <label for="">Số giường bệnh</label>
            <input type="number" name="editBed_number" class="form-control" value="${item.bed_number}">
        </div>
        <div>
            <button type="button" class="btn btn-primary" onclick="realUpdate('${doc.id}')">Lưu</button>
        </div>`
        document.querySelector('.edit').innerHTML = content;
    })
}

function realUpdate(key) {
    db.collection("hospitals").doc(key).update({
        name: document.querySelector('input[name="editName"]').value,
        address: document.querySelector('input[name="editAddress"]').value,
        bed_number: document.querySelector('input[name="editBed_number"]').value,
        logo: document.querySelector('input[name="editLogo"]').value,
    })
}

function savePatient() {

    let data = {
        patientName: document.querySelector('input[name="patientName"]').value,
        age: document.querySelector('input[name="age"]').value,
        bed_no: document.querySelector('input[name="bed_no"]').value,
        description: document.querySelector('input[name="description"]').value,
        hospital_id: document.querySelector('select[name="hospital_id"]').value,
    };
    var newpPatient = db.collection("patients").doc();
    newpPatient.set(data);
    document.querySelector('input[name="patientName"]').value = "";
    document.querySelector('input[name="age"]').value = "";
    document.querySelector('input[name="bed_no"]').value = "";
    document.querySelector('input[name="description"]').value = "";
    document.querySelector('select[name="hospital_id"]').value = "";
}

function reload() {
    location.reload();
}

function renderPatient(key) {
    console.log(key);
    db.collection("patients").where("hospital_id", "==", key + "").get()
        .then((querySnapshot) => {
            let content = ``;
            querySnapshot.forEach((doc) => {
                let item = doc.data();
                content += `<tr>
                <td>${item.patientName}</td>
                <td>${item.age}</td>
                <td>${item.description}</td>
                <td>${item.bed_no}</td>
                <td><button class="btn btn-primary" onclick="removePatient('${doc.id}')">Xoá</button></td>
                <td><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal4" onclick="updatePatient('${doc.id}')">Sửa</button></td>
            </tr>`;
            });
            $('.patients').append(content);
        })
        // onSnapshot(function(querySnapshot) {
        //         if (querySnapshot.exists) {
        //             let content = ``;
        //             querySnapshot.forEach(function(doc) {
        //                 console.log(doc.id, " => ", doc.data());
        //                 var userData = doc.data()
        //                 var userId = doc.id
        //             });
        //         } else {

    //         };

    // querySnapshot.forEach(function(doc) {
    //     content += `<tr>
    //     <td>${item.patientName}</td>
    //     <td>${item.age}</td>
    //     <td>${item.description}</td>
    //     <td>${item.bed_no}</td>
    //     <td><button class="btn btn-primary" onclick="removePatient('${doc.id}')">Xoá</button></td>
    //     <td><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal2" onclick="updatePatient('${doc.id}')">Sửa</button></td>
    // </tr>`;
    // });
    // $('.patients').append(content);
    // console.log(querySnapshot.data());
}

function removePatient(key) {

    db.collection("patients").doc(key).delete().then(function() {
        location.reload();
    })


}

function updatePatient(key) {
    var docRef = db.collection("patients").doc(key);

    docRef.get().then(function(doc) {
        content = ``;
        item = doc.data();
        content = `<legend>Sửa Thông Tin</legend>
        <div class="form-group">
            <label for="">Tên bệnh nhân</label>
            <input type="text" name="editPatientName" class="form-control" value="${item.patientName}">
        </div>
        <div>
            <label for="">Tuổi</label>
            <input type="number" name="editAge" class="form-control" value="${item.age}">
        </div>
        <div>
            <label for="">Mô Tả</label>
            <input type="text" name="editDescription" class="form-control" value="${item.description}">
        </div>
        <div>
            <label for="">Số giường bệnh</label>
            <input type="number" name="editBed_no" class="form-control" value="${item.bed_no}">
        </div>
        <div>
            <label for="">Bệnh viện</label>
            <select class="select" name="hospital_id"></select>
        </div>
        <div>
            <button type="button" class="btn btn-primary" onclick="realUpdate('${doc.id}')">Lưu</button>
        </div>`
        document.querySelector('.editPatient').innerHTML = content;
    })
}

function realUpdatePatient(key) {
    db.collection("patients").doc(key).update({
        patientName: document.querySelector('input[name="editPatientName"]').value,
        age: document.querySelector('input[name="editAge"]').value,
        bed_no: document.querySelector('input[name="editBed_no"]').value,
        description: document.querySelector('input[name="editDescription"]').value,
        hospital_id: document.querySelector('select[name="hospital_id"]').value,
    })
}